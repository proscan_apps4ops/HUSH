import datetime
#print("1:", datetime.datetime.now())
import sys
import time

from qtpy.QtCore import Qt, QTime, QTimer
from qtpy.QtGui import QColor, QImage, QPainter, QPixmap
from qtpy.QtWidgets import QApplication, QLabel

from pyqtacc.qrc_resources.facility.proscan.pyrcc5 import qrc_resources

#print("1b:", datetime.datetime.now())
app = QApplication(sys.argv)
#print("2:", datetime.datetime.now())
try:
    due = QTime.currentTime()
    message = "HUSH! will begin to load shortly"
    if len(sys.argv) < 2:
        raise ValueError
    hours, mins = sys.argv[1].split(":")
    due = QTime(int(hours), int(mins))
    if not due.isValid():
        raise ValueError
    if len(sys.argv) > 2:
        message = " ".join(sys.argv[2:])
except ValueError:
    message = "Usage: wakeup.py HH:MM [optional message]"

#print("3:", datetime.datetime.now())
while QTime.currentTime() < due:
    time.sleep(1)
#print("4:", datetime.datetime.now())
image = QImage(":/Hush.jpg")
p = QPainter(image)
font = p.font()
font.setPixelSize(54)
p.setFont(font)
p.setPen(QColor(Qt.red))
#p.setStyleSheet("color:red;")
#p.save()
p.drawText(40, 350, message)
#p.restore()

#pixmap = QPixmap(":/Hush.jpg")
pixmap = QPixmap.fromImage(image)
#pixmap.scaled(80, 80, Qt.IgnoreAspectRatio)
label = QLabel() #"<font color=red size=72><b>" + message + "</b></font>")
label.setPixmap(pixmap)
label.setScaledContents(True)
#label.setMask(pixmap.mask())
#label.setStyleSheet("border-image: url(:/Hush.jpg) 0 0 0 0 stretch stretch;")
#label.setText("<img width=80px height=80px src=\":/Hush.jpg\"/>")
#label.setText("<font color=red size=72><b>" + message + "</b></font>")
label.setWindowFlags(Qt.SplashScreen)
label.setAlignment(Qt.AlignCenter)
label.setFixedHeight(600)
label.setFixedWidth(600)
label.show()
QTimer.singleShot(5000, app.quit) #5 seconds
#print("5:", datetime.datetime.now(), flush=True)
app.exec_()
