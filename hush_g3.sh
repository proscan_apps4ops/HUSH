#!/bin/bash
cd /proscan/bd/applications/hush/hla/1.5.0

# For use if script is sourced rather than executed
appNameDefault="hush_g3.sh"
configBase="config"
configSector=${configBase}/g3
echo "${configSector}"

module unload gcc
module load gcc/7.5.0

if [ -z "$PS1" ] # no prompt?
### if [ -v PS1 ]   # On Bash 4.2+ ...
then
  # non-interactive
  echo "Non-interactive script"
else
  # interactive
  echo "Interactive/sourced script"
fi

_EPICS_HOST_ARCH=${RHREL}-x86_64
_EPICS_BASE=base-7.0.8

# Select Python Version here. Currently one of 3.5, 3.7, 3.8, 3.10
PYTHON_VERSION=3.10

# Only neede for verison-specific packages, e.g., bigtree-
# Not required for elog.
PYTHON_VERSION_DIR=310
EXTRA_PYTHON_PATH_FLAG=0

if [ "$1" ]; then
    
   if [ "$1" == "3.5" -o "$1" == "35" ]; then
      echo "Using default version $PYTHON_VERSION"
   elif [ "$1" == "3.7" -o "$1" == "37" ]; then
       PYTHON_VERSION=3.7
       PYTHON_VERSION_DIR=37
       export LD_PRELOAD=/usr/local/epics/${_EPICS_BASE}/lib/${_EPICS_HOST_ARCH}/libca.so:/usr/local/epics/${_EPICS_BASE}/lib/${_EPICS_HOST_ARCH}/libCom.so
       
   elif [ "$1" == "3.8" -o "$1" == "38" ]; then
       PYTHON_VERSION=3.8
       PYTHON_VERSION_DIR=38
       #EXTRA_PYTHON_PATH_FLAG=1
   elif [ "$1" == "3.10" -o "$1" == "310" ]; then
       PYTHON_VERSION=3.10
       PYTHON_VERSION_DIR=310
       #EXTRA_PYTHON_PATH_FLAG=1
   else
      echo "Requested Python version is not supported"
      echo "Using default version $PYTHON_VERSION"
   fi  

fi
 
echo "PYTHON_VERSION $PYTHON_VERSION"

. /opt/gfa/python $PYTHON_VERSION

#/sf/bd/applications/OnlineModel/current/PythonModule:/sf/bd/applications/OnlineModel/current
#/sf/bd/applications/OnlineModel/default/scripts/VA:

#C_EXT version for Py 3.7:
export PYTHONPATH=.:/opt/gfa/cafe/python/pycafe/cafe-1.21.0/lib/${_EPICS_HOST_ARCH}:/proscan/bd/applications/deps/apps4ops/v1.10.0

if [ "${EXTRA_PYTHON_PATH_FLAG}" -gt  "0" ] ; then
    export PYTHONPATH=$PYTHONPATH:/proscan/bd/applications/deps/apps4ops/v1.10.0/common/python${PYTHON_VERSION_DIR}
fi
echo $PYTHONPATH

STDOUT_DIR="/tmp" 
#Set to 0 if you do not wish std::out to write to logger
STDOUT_FLAG=1

if [ "$#" -gt "0" ]; then
    appName=$0
    if [ "$appName" == "-bash" ]; then
	appName=$appNameDefault
    fi
else
appName=$appNameDefault
fi

#echo "$appName $@ $#"
baseName="${appName##*/}"
name=$(echo "$baseName" | cut -f 1 -d '.')
nameconfig=$(echo "$baseName" | cut -f 1 -d '_')
nameLog=${name}-$USER
echo "Application: $name"
echo "nameLog: $nameLog" 

#Configuration files can be overwritten
#python ${name}.py -s="/sf/bd/deps/pyqtacc/common/config/style.json" -f="/sf/bd/deps/pyqtacc/v1.0.0/pyqtacc/qrc_resources/facility/sf/config/base.json" -q="/sf/bd/deps/pyqtacc/common/config/acc.qss" &

if [ "${STDOUT_FLAG}" -gt  "0" ] ; then
#if changing the std::out destination, remember to also change it in your json config file (for reading)
    if test -f "$STDOUT_DIR/${nameLog}.log---"; then
	rm -f $STDOUT_DIR/${nameLog}.log---
    fi
    if test -f "$STDOUT_DIR/${nameLog}.log--"; then
	cp $STDOUT_DIR/${nameLog}.log-- $STDOUT_DIR/${nameLog}.log---
	rm -f $STDOUT_DIR/${nameLog}.log--
    fi
    if test -f "$STDOUT_DIR/${nameLog}.log-"; then
	cp $STDOUT_DIR/${nameLog}.log- $STDOUT_DIR/${nameLog}.log--
	rm -f $STDOUT_DIR/${nameLog}.log-
    fi
    if test -f "$STDOUT_DIR/${nameLog}.log"; then
	cp $STDOUT_DIR/${nameLog}.log $STDOUT_DIR/${nameLog}.log-
	rm -f $STDOUT_DIR/${nameLog}.log
    fi

    python wakeup.py 00:00 &
    python ${name}.py -u ${configSector}/${nameconfig}.json >> $STDOUT_DIR/${nameLog}.log 2>&1 &
    chmod 777 $STDOUT_DIR/${nameLog}.log
else
#Run instead without std::out
    python ${name}.py -u ${configSector}/${nameconfig}.json &
fi

if [ "${PYTHON_VERSION}" == "3.7" ] ; then
    unset LD_PRELOAD
fi
