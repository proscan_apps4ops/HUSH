"""Hush.py module for energy saving accounting
"""
import inspect
import os
import platform
import sys
import time

from qtpy.QtCore import __version__ as QT_VERSION_STR
from qtpy.QtCore import (
    PYQT_VERSION_STR, Signal, Slot)
from qtpy.QtWidgets import QApplication, QMessageBox

from apps4ops.bdbase.base import BaseWindow
from apps4ops.bdbase.enumkind import UserMode, Facility
from apps4ops.bdbase.helpbrowser import HelpBrowser
from apps4ops.proscan.enumkind import ElogPROSCAN
from apps4ops.proscan.sendelogproscan import QSendToELOG
from pyrcc5 import hush_resources 
from src.gui import AppGui

_pymodule = os.path.basename(__file__)
_appname, _appext = _pymodule.split(".")
_appversion = "2.0.0"
# _title = """Power Usage & Saving Hierarchy""" #, HUSH!"""
_title = """HUSH!"""


def _line():
    """Macro to return the current line number.

    The current line number within the file is used when
    reporting messages to the message logging window.

    Returns:
        int: Current line number.
    """
    return inspect.currentframe().f_back.f_lineno


class StartMain(BaseWindow):
    """Main class, inherits from apps4ops BaseWindow
    """
    trigger_log_message = Signal(str, str, int, str, dict)

    def __init__(self, parent=None):
        # super(StartMain, self).__init__(
        # Using Python 3 style super() without arguments
        super().__init__(
            parent=parent, pymodule=_pymodule, appversion=_appversion,
            title=_title, user_mode=UserMode.OPERATION,
            facility=Facility.PROSCAN, has_optics=False, has_procedure=False)

        self.appname = _appname
        self.title = _title
        self.elog_enum = ElogPROSCAN()
        self.gui = AppGui(self)

        self.setMinimumHeight(974)
        self.setMinimumWidth(1460)
        
    def prepare_elog_message(self):
  
        self.system_idx = self.elog_enum.system.NONE
        self.eintrag_idx = self.elog_enum.eintrag.INFO
        self.effekt_idx = self.elog_enum.effekt.NO

        self.attach_files = []

        simulation = self.input_parameters["simulation"]

        if self.all_data:
            if self.all_data["Input data"] is not None:
                try:
                    simulation = self.all_data["Input data"]["simulation"]
                except KeyError:
                    simulation = self.input_parameters["simulation"]
                    pass

        self.logbook = "Sandkasten" if simulation else "Proscan"
       
        sector = ["<br>G1: ", "<br>SH: ", "<br>G2: ", "<br>O2: ", "<br>PIF: ",
                  "<br>G3: ", "<br>------------------<br>Tot: "]
        pvlist = ["PG1-HUSH:TOTSAVE", "PSH-HUSH:TOTSAVE", "PG2-HUSH:TOTSAVE",
                  "PO2-HUSH:TOTSAVE", "PPIF-HUSH:TOTSAVE", "PG3-HUSH:TOTSAVE",
                  "PRO-HUSH:TOTSAVE"]
        value, stat, stat_list = self.cafe.getScalarList(
            pvlist, cacheFlag=True)

        if stat != self.cyca.ICAFE_NORMAL:
            self.check_status_list(_pymodule, "getScalarListCache",
                                   pvlist, stat_list, _line())

        message = ("Power saved for the current year stands at " +
                   "{:.3f} MWh:").format(value[-1])
        for label, val in zip(sector, value):
            message += label + "{:.3f} MwH".format(val)

        self.message = message
        print(self.message, flush=True)

    @Slot()
    def send_to_elog(self):

        @Slot()
        def save_fig_thread_finished():

            time.sleep(0.2)

            if self.all_data:
                QSendToELOG(self, logbook=self.logbook,
                            eintragIdx=self.eintrag_idx,
                            systemIdx=self.system_idx,
                            effektIdx=self.effekt_idx,
                            title=self.title,
                            message=self.message,
                            attachFile=self.attach_files)

            time.sleep(1.0)

        # if not self.verify_send_to_elog():
        #    return

        self.prepare_elog_message()

        print(self.message, flush=True)

        if not self.all_data:
            QSendToELOG(self, logbook=self.logbook,
                        eintragIdx=self.eintrag_idx,
                        systemIdx=self.system_idx,
                        effektIdx=self.effekt_idx,
                        title=self.title,
                        message=self.message,
                        attachFile=self.attach_files)
            return

        folder_name = self.elog_dest
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

        time_in_seconds = 0
        '''
        time_in_seconds = self.all_data['Ambient data']['Time in seconds']
        if self.all_data['Processed data']['Reanalysis time']:
            reanalysis_time = self.all_data['Processed data'][
                'Reanalysis time in seconds']
        else:
            reanalysis_time = None
        '''

        reanalysis_time = None

        self.folder_name = folder_name

        save_fig_thread = self.SaveFigureThread(
            self, self.folder_name, time_in_seconds, reanalysis_time)
        save_fig_thread.finished.connect(save_fig_thread_finished)

        save_fig_thread.start()
        time.sleep(0.05)

    @Slot()
    def closeEvent(self, event):

        if not self.verify_close_event():
            event.ignore()
            return

        BaseWindow.closeEvent(self, event)

    @Slot()
    def show_about(self):
        """ To overide by application
        """
        QApplication.processEvents()
        QMessageBox.about(
            self, "About",
            """<b>{0}</b> v {1}
            <p>Copyright &copy; Paul Scherrer Institut (PSI).
            All rights reserved.</p>
            <p>Author: J. Chrin, February 2024 </p>
            <p>(EPICS db adapted from A. Kovach, 2016) </p>
            <p>IOC Administrator: P. Fernandez (prev. H. Lutz) </p>
            <p>1st Responsible: J. Chrin  </p>
            <p>Initiates energy saving procedures and records power saved</p>
            <p>Python {2} - Qt {3} - PyQt {4} <br>
            cafe {5} - epics {6} on {7}""".format(
                _pymodule, _appversion, platform.python_version(),
                QT_VERSION_STR, PYQT_VERSION_STR,
                self.cafe.CAFE_version(), self.cafe.EPICS_version(),
                platform.system()))
        QApplication.processEvents()

    @Slot()
    def show_help(self):
        """ Invoke help pages from hush_resources
        """
        index_html = "index.html"  # self.appname + "/index.html"
        help_base = ":"  # + self.appname
        help_page = HelpBrowser(help_base, index_html, self)
        help_page.show()


#########################################################################
if __name__ == "__main__":

    delay = 25 if "_" in _pymodule else 40
    app = QApplication(sys.argv)
    splash = BaseWindow.initialize_application(
        app, appname=_appname, delay=delay, facility=Facility.PROSCAN)

    myapp = StartMain()

    myapp.show()

    if splash is not None:
        splash.finish(myapp)

    app.exec_()
